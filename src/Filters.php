<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Collection;

class Filters
{
    /**
     * @var Container
     */
    static public $container;

    protected $filters;

    public function __construct($filters)
    {
        $this->filters = $this->wrap($filters);
    }

    public function __invoke($data)
    {
        return (new Collection($data))
            ->map([$this, 'filter'])
            ->toArray();
    }

    public function filter($value, $key)
    {
        if (is_null($value)) {
            return null;
        }

        return $this->filters->get($key, new Collection)
            ->map(function ($filter) {
                return Filter::parse($filter);
            })
            ->reduce(function ($value, $filter) {
                return $filter($value);
            }, $value);
    }

    protected function wrap($filters)
    {
        return (new Collection($filters))
            ->map(function ($filters) {
                return new Collection(array_wrap($filters));
            });
    }
}
